import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  constructor(private httpClient: HttpClient) { }

  public getAllPlanets() {
    return this.httpClient.get('https://swapi.co/api/planets');
  }

}
