import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-planetes',
  templateUrl: './planetes.component.html',
  styleUrls: ['./planetes.component.scss'],
})
export class PlanetesComponent implements OnInit {

    planetes: Observable<any>;

  constructor(private http: HttpClient) { }
  ngOnInit() {
  }

}
